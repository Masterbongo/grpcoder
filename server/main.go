package main

import (
	"context"
	"log"
	"net"
	"strings"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"

	"example.com/masamerc/grpcoder/encoder"
)

const port = ":8080"

type Server struct {
	encoder.UnimplementedEncoderServer
}

func (s *Server) Encode(ctx context.Context, in *encoder.EncodeRequest) (*encoder.EncodeResponse, error) {
	log.Println("Received request to encode:", in.String_)

	source := []byte(in.String_)
	var result string

	switch in.EncodingType {
	case encoder.EncodingType_BASE64:
		result = encoder.ConvertToBase64(source)
	case encoder.EncodingType_BASE32:
		result = encoder.ConvertToBase32(source)
	case encoder.EncodingType_HEX:
		result = encoder.ConvertToHex(source)
	case encoder.EncodingType_BIN:
		result = encoder.ConvertToBinary(source)
	default:
		return nil, status.Errorf(codes.InvalidArgument, "Unknown encoding type: %v", in.EncodingType)
	}

	return &encoder.EncodeResponse{Result: result}, nil
}

func main() {
	listener, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	// prep grpc server and encoder service
	svc := &Server{}
	server := grpc.NewServer()
	encoder.RegisterEncoderServer(server, svc)
	reflection.Register(server)

	// start grpc Server
	log.Printf("gRPC server started on port %s\n", strings.Replace(port, ":", "", 1))
	if err := server.Serve(listener); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
