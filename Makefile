generate:
	protoc -I proto proto/*.proto --go_out=encoder --go_opt=paths=source_relative \
    --go-grpc_out=encoder --go-grpc_opt=paths=source_relative

run_server: generate
	go run server/main.go

run_client: generate
	go run client/main.go
