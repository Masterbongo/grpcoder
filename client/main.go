package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"example.com/masamerc/grpcoder/encoder"
)

func main() {
	conn, err := grpc.Dial(
		"localhost:8080",
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	// get client
	client := encoder.NewEncoderClient(conn)

	// prep request
	encoderRequestSum := &encoder.EncodeRequest{
		String_:      "hello",
		EncodingType: encoder.EncodingType_BASE64,
	}

	// invoke
	ctx := context.Background()
	ctxWithTimeout, cancel := context.WithTimeout(ctx, 100*time.Millisecond)
	defer cancel()

	resp, err := client.Encode(ctxWithTimeout, encoderRequestSum)
	if err != nil {
		log.Fatalf("could not encode: %v", err)
	}
	fmt.Printf("response from Encode: %s\n", resp.Result)
}
