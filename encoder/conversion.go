package encoder

import (
	"encoding/base32"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"strings"
)

func ConvertToBase64(source []byte) string {
	return base64.StdEncoding.EncodeToString(source)
}

func ConvertToBase32(source []byte) string {
	return base32.StdEncoding.EncodeToString(source)
}

func ConvertToHex(source []byte) string {
	return hex.EncodeToString(source)
}

func ConvertToBinary(source []byte) string {
	buf := strings.Builder{}

	for _, byteVal := range source {
		// Format each byte as a binary string with leading zeros to ensure 8 bits.
		// Use fmt.Sprintf with %08b to format the string with leading zeros.
		binaryString := fmt.Sprintf("%08b", byteVal)
		buf.WriteString(binaryString)
	}

	return buf.String()
}
